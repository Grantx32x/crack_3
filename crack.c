#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "bintree.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    if(hash == md5(guess, HASH_LEN))
    {
        return 1;
    }
    free(hash);
    return 0;
}

// TODO
// Read in the hash file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **read_hashes(char *filename)
{
    int arrlen = 50;
    char **arr = (char **)malloc(50 * sizeof(char *));
    
    FILE *f = fopen(filename, "r");
    
    if(!f)
    {
        printf("Can't open %s\n", "hashes.txt");
        exit(1);
    }
    
    char line[40];
    int i = 0;
    while(fscanf(f, "%s", line) != EOF)
    {
        arr[i] = malloc((strlen(line)+1) * sizeof(char));
        strcpy(arr[i], line);
        i++;
        if(i == arrlen)
        {
            // Make array 25% bigger
            arrlen = (int)(arrlen * 1.25);
            char **newarr = realloc(arr, arrlen * sizeof(char *));
            if(newarr != NULL) arr = newarr;
            else exit(3);
        }
    }
    
    // Add NULL to end of array to mark end
    arr[i] = NULL;
    return arr;
}


// TODO
// Read in the dictionary file and return the tree.
// Each node should contain both the hash and the
// plaintext word.
node *read_dict(char *filename)
{
    char hash[33];
    char pass[50];
    FILE *f = fopen(filename, "r");

    node *root = NULL;
    char line[50];
    while(fscanf(f, "%s", line) != EOF)
    {
        insert(pass, hash, &root);
    }
    
    fclose(f);
    return root;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into a binary tree
    node *dict = read_dict(argv[2]);

    // TODO
    // For each hash, search for it in the binary tree.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    FILE *fd = fopen(argv[2], "r");
    
    node *tree = NULL;
    while(fscanf(fd, "%s", (char *)dict) != EOF)
    {
        search(*hashes, tree);
    }
    fclose(fd);
    
    print(tree);
    return 0;
}
